" Set file encoding
set encoding=utf-8
scriptencoding utf-8

filetype plugin indent on

"Press Leader with both of my thumbs, and my fingers are always on home row
let g:mapleader = "\<Space>"

" Automatic installation vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" define a group `vimrc` and initialize
augroup vimrc
    autocmd!
augroup END
" Register autocmds to group `vimrc`
  autocmd vimrc VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" =============================================================================
" Appearance
" =============================================================================

" Relative linenumber
set relativenumber

" Color_coded
Plug 'jeaye/color_coded'
" This shit need gvim to work -> setting guioptions
set guioptions='a'

" Molokai
Plug 'https://github.com/tomasr/molokai'
syntax enable
set background=dark
let g:molokai_original = 1
colorscheme molokai

" vim-devicons
set guifont=SauceCodePro+Powerline+AwesomeRegular\ Nerd\ Font 

" Cursor line
set cursorline

" vim-airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" {{{
let g:powerline_loaded = 1
set laststatus=2
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.whitespace = 'Ξ'

" Tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#show_tabs = 0
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#tabline#fnamecollapse = 1
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#buffer_min_count = 2


nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9

let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#hunks#enabled = 1
let g:airline#extensions#hunks#non_zero_only = 0
let g:airline#extensions#hunks#hunk_symbols = ['+', '~', '-']
let g:airline#extensions#wordcount#enabled = 1
let g:airline#extensions#ctrlp#color_template = 'insert'
let g:airline#extensions#tagbar#flags = 'p'
let g:airline_theme='wombat'

" }}}

" The fancy start screen for Vim. 
Plug 'mhinz/vim-startify'
" {{{
  let g:startify_session_dir                    = '~/.vim/session'
  let g:startify_list_order                     = ['sessions']
  let g:startify_session_persistence            = 1
  let g:startify_session_delete_buffers         = 1
  let g:startify_change_to_dir                  = 1
  let g:startify_change_to_vcs_root             = 1
  let g:startify_bookmarks                      = 1
  let g:startify_relative_path                  = 1

  " TODO - startify_bookmarks

  nnoremap <F2> :Startify<CR>
augroup startify
    autocmd!
augroup END
  autocmd startify User Startified setlocal colorcolumn=0
" }}}

Plug 't9md/vim-choosewin'
" {{{
  nmap - <Plug>(choosewin)
  let g:choosewin_blink_on_land = 0
  let g:choosewin_tabline_replace = 0
  let g:choosewin_overlay_enable = 1
" }}}

" The NERD Tree:  A tree explorer plugin for vim
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Open NERDTree with Ctrl+n
map <C-n> :NERDTreeToggle<CR>

" Vim devicons
Plug 'ryanoasis/vim-devicons'

" Better Rainbow Parentheses
Plug 'kien/rainbow_parentheses.vim'

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

let g:rbpt_max = 16

let g:rbpt_loadcmd_toggle = 0

augroup rainbow
    autocmd!
augroup END
au rainbow VimEnter * RainbowParenthesesToggle
au rainbow Syntax * RainbowParenthesesLoadRound
au rainbow Syntax * RainbowParenthesesLoadSquare
au rainbow Syntax * RainbowParenthesesLoadBraces

" =============================================================================
" Completion
" =============================================================================

" Deoplete for nvim
if has('nvim')
    function! DoRemote(arg)
        UpdateRemotePlugins
    endfunction
    Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
endif

" YouCompleteMe: a code-completion engine for Vim
function! BuildYCM(info)
  " info is a dictionary with 3 fields
  " - name:   name of the plugin
  " - status: 'installed', 'updated', or 'unchanged'
  " - force:  set on PlugInstall! or PlugUpdate!
  if a:info.status ==? 'installed' || a:info.force
    !./install.py --clang-completer --gocode-completer 
  endif
endfunction

Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM'), 'for': ['cpp', 'go'] } 
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_complete_in_strings = 1
let g:ycm_server_python_interpreter = '/usr/bin/python'

" -----------------------------------
" YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_extra_conf.py'
let g:ycm_key_detailed_diagnostics = '<leader>w'
let g:ycm_open_loclist_on_ycm_diags = 0
let g:ycm_show_diagnostics_ui = 0
let g:ycm_auto_trigger = 1
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_echo_current_diagnostic = 0
let g:ycm_warning_symbol = '∆'
let g:ycm_error_symbol = '✗'
let g:ycm_enable_diagnostic_signs = 0
let g:ycm_enable_diagnostic_highlighting = 0
let g:ycm_always_populate_location_list = 0
let g:ycm_complete_in_comments = 0
let g:ycm_complete_in_strings = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_seed_identifiers_with_syntax = 0
let g:ycm_server_use_vim_stdout = 0
let g:ycm_server_keep_logfiles = 0
let g:ycm_server_log_level = 'info'
let g:ycm_add_preview_to_completeopt = 0
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_max_diagnostics_to_display = 30
let g:ycm_key_list_select_completion = ['<tab>']
let g:ycm_key_list_previous_completion = ['<s-tab>']
let g:ycm_key_invoke_completion = '<C-Space>'
let g:ycm_seed_identifiers_with_syntax = 1

let g:ycm_cache_omnifunc = 1
let g:ycm_use_ultisnips_completer = 1
let g:ycm_goto_buffer_command = 'same-buffer'
let g:ycm_disable_for_files_larger_than_kb = 1000

let g:ycm_register_as_syntastic_checker = 1

let g:ycm_filetype_whitelist = {
            \ '*': 1
            \}

let g:ycm_filetype_blacklist = {
            \ 'tagbar': 1,
            \ 'qf': 1,
            \ 'notes': 1,
            \ 'markdown': 1,
            \ 'unite': 1,
            \ 'text': 1,
            \ 'vimwiki': 1,
            \ 'pandoc': 1,
            \ 'infolog': 1,
            \ 'mail': 1
            \}

let g:ycm_filetype_specific_completion_to_disable = {
            \ 'gitcommit': 1
            \}

let g:ycm_semantic_triggers =  {
            \   'c' : ['->', '.'],
            \   'objc' : ['->', '.'],
            \   'ocaml' : ['.', '#'],
            \   'cpp,objcpp' : ['->', '.', '::'],
            \   'perl' : ['->'],
            \   'php' : ['->', '::'],
            \   'cs,java,javascript,d,python,perl6,scala,vb,elixir,go' : ['.'],
            \   'vim' : ['re![_a-zA-Z]+[_\w]*\.'],
            \   'ruby' : ['.', '::'],
            \   'lua' : ['.', ':'],
            \   'erlang' : [':'],
            \ }

" YCM-Generator
Plug 'rdnetto/YCM-Generator', {'branch': 'stable'}

" UltiSnips - The ultimate snippet solution for Vim
Plug 'SirVer/ultisnips' 
" Trigger configuration.
" Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsListSnippets = '<nop>'
let g:UltiSnipsExpandTrigger='<c-j>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'
let g:ulti_expand_or_jump_res = 0
let g:UltiSnipsEditSplit = 'horizontal'

" ------ Auto load YCM && UltilSnips first time enter Insert mode
augroup load_us_ycm
    autocmd!
    autocmd InsertEnter * call plug#load('ultisnips', 'YouCompleteMe')
                        \| autocmd! load_us_ycm
augroup END

" Vim-snippets
Plug 'honza/vim-snippets'

" Surround
Plug 'jiangmiao/auto-pairs'

" =============================================================================
" File Navigation
" =============================================================================

" fzf: command-line fuzzy finder written in Go
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" {{{
  let g:fzf_nvim_statusline = 0 " disable statusline overwriting

  " TODO
  nnoremap <silent> <leader>e               :Files<CR>
  nnoremap <silent> <leader>a               :Buffers<CR>
  nnoremap <silent> <leader>;               :BLines<CR>
  nnoremap <silent> <leader>.               :Lines<CR>
  nnoremap <silent> <leader>o               :BTags<CR>
  nnoremap <silent> <leader>O               :Tags<CR>
  nnoremap <silent> <leader>?               :History<CR>
  nnoremap <silent> <leader>/               :execute 'Ag ' . input('Ag/')<CR>
  nnoremap <silent> K                       :call SearchWordWithAg()<CR>
  vnoremap <silent> K                       :call SearchVisualSelectionWithAg()<CR>
  nnoremap <silent> <leader>gl              :Commits<CR>
  nnoremap <silent> <leader>ga              :BCommits<CR>

  imap <C-x><C-f> <plug>(fzf-complete-file-ag)
  imap <C-x><C-l> <plug>(fzf-complete-line)

  function! SearchWordWithAg()
    execute 'Ag' expand('<cword>')
  endfunction

  function! SearchVisualSelectionWithAg() range
    let old_reg = getreg('"')
    let old_regtype = getregtype('"')
    let old_clipboard = &clipboard
    set clipboard&
    normal! ""gvy
    let selection = getreg('"')
    call setreg('"', old_reg, old_regtype)
    let &clipboard = old_clipboard
    execute 'Ag' selection
  endfunction
" }}}

" Vim tagbar - display tags in a window
Plug 'majutsushi/tagbar'

nmap <F3> :TagbarToggle<CR>

" A vim script - quick switch between .c && .h
Plug 'vim-scripts/a.vim'

" =============================================================================
" Languages
" =============================================================================

" Syntastic: Code linting errors
" TODO
Plug 'scrooloose/syntastic', { 'for': ['c', 'cpp', 'lua', 'markdown', 
                                \ 'json', 'python', 'javascript',
                                \ 'vim', 'zsh', 'css'] }
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list            = 1
let g:syntastic_check_on_open            = 1
let g:syntastic_enable_signs             = 1
let g:syntastic_enable_highlighting      = 1
let g:syntastic_cpp_check_header         = 1
let g:syntastic_enable_balloons          = 1
let g:syntastic_echo_current_error       = 1
let g:syntastic_check_on_wq              = 1
let g:syntastic_error_symbol             = '✘'
let g:syntastic_warning_symbol           = '!'
let g:syntastic_style_error_symbol       = ':('
let g:syntastic_style_warning_symbol     = ':('
let g:syntastic_vim_checkers             = ['vint']
let g:syntastic_python_checkers          = ['flake8']
let g:syntastic_javascript_checkers      = ['eslint']
let g:Syntastic_c_checkers               = ['cppchecker']
let g:Syntastic_lua_checkers             = ['luac', 'luacheck']
let g:Syntastic_markdown_checkers        = ['mdl']
let g:Syntastic_json_checkers            = ['jsonlint']
let g:Syntastic_css_checkers             = ['csslint']
let g:Syntastic_zsh_checkers             = ['zsh']
let g:syntastic_enable_elixir_checker    = 0

" Go development plugin for Vim
Plug 'fatih/vim-go', { 'for': 'go' }
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }

let g:go_fmt_command = 'goimports'

let g:go_bin_path = expand('~/.local/bin')

augroup go
    autocmd!
augroup END

au go FileType go nmap <leader>r <Plug>(go-run)
au go FileType go nmap <leader>b <Plug>(go-build)
au go FileType go nmap <leader>t <Plug>(go-test)
au go FileType go nmap <leader>c <Plug>(go-coverage)
au go FileType go nmap <Leader>ds <Plug>(go-def-split)
au go FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au go FileType go nmap <Leader>dt <Plug>(go-def-tab)
au go FileType go nmap <Leader>gd <Plug>(go-doc)
au go FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au go FileType go nmap <Leader>gb <Plug>(go-doc-browser)
au go FileType go nmap <Leader>s <Plug>(go-implements)
au go FileType go nmap <Leader>i <Plug>(go-info)
au go FileType go nmap <Leader>e <Plug>(go-rename)

" vim systemd service syntax
Plug 'Matt-Deacalion/vim-systemd-syntax'

" Markdown Vim Mode
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
let g:vim_markdown_folding_disabled=1  " Disable Folding
let g:vim_markdown_math=1  " LaTeX math
let g:vim_markdown_frontmatter=1 " YAML

" =============================================================================
" Git
" =============================================================================

" Fugitive: Git from within Vim
Plug 'tpope/vim-fugitive'
" {{{
" TODO
  nnoremap <silent> <leader>gs :Gstatus<CR>
  nnoremap <silent> <leader>gd :Gdiff<CR>
  nnoremap <silent> <leader>gc :Gcommit<CR>
  nnoremap <silent> <leader>gb :Gblame<CR>
  nnoremap <silent> <leader>ge :Gedit<CR>
  nnoremap <silent> <leader>gE :Gedit<space>
  nnoremap <silent> <leader>gr :Gread<CR>
  nnoremap <silent> <leader>gR :Gread<space>
  nnoremap <silent> <leader>gw :Gwrite<CR>
  nnoremap <silent> <leader>gW :Gwrite!<CR>
  nnoremap <silent> <leader>gq :Gwq<CR>
  nnoremap <silent> <leader>gQ :Gwq!<CR>

  function! ReviewLastCommit()
    if exists('b:git_dir')
      Gtabedit HEAD^{}
      nnoremap <buffer> <silent> q :<C-U>bdelete<CR>
    else
      echo 'No git a git repository:' expand('%:p')
    endif
  endfunction
  nnoremap <silent> <leader>g` :call ReviewLastCommit()<CR>

  augroup fugitiveSettings
    autocmd!
    autocmd FileType gitcommit setlocal nolist
    autocmd BufReadPost fugitive://* setlocal bufhidden=delete
  augroup END
" }}}

" GitGutter
Plug 'airblade/vim-gitgutter'
" {{{
  let g:gitgutter_sign_added = '✚'
  let g:gitgutter_sign_modified = '⚡'
  let g:gitgutter_sign_removed = '✖'
  let g:gitgutter_map_keys = 0
  let g:gitgutter_max_signs = 200
  let g:gitgutter_realtime = 0
  let g:gitgutter_eager = 0
  let g:gitgutter_diff_args = '--ignore-space-at-eol'
  nmap <silent> ]h :GitGutterNextHunk<CR>
  nmap <silent> [h :GitGutterPrevHunk<CR>
  nnoremap <silent> <Leader>gu :GitGutterRevertHunk<CR>
  nnoremap <silent> <Leader>gp :GitGutterPreviewHunk<CR><c-w>j
  nnoremap cog :GitGutterToggle<CR>
  nnoremap <Leader>gt :GitGutterAll<CR>
" }}}

" =============================================================================
" Misc
" =============================================================================

Plug 'rking/ag.vim'
let g:ag_working_path_mode='r'

" Make commenting easier
Plug 'tpope/vim-commentary'

" Split navigation that works with tmux
" Plug 'christoomey/vim-tmux-navigator'

" virtualenv support for vim
Plug 'jmcantrell/vim-virtualenv'

" SimpylFold
Plug 'tmhedberg/SimpylFold'


" TODO
" Other plugins require curl
if executable('curl')

    " Webapi: Dependency of Gist-vim
    Plug 'mattn/webapi-vim'

    " Gist: Post text to gist.github
    Plug 'mattn/gist-vim'
endif

filetype plugin indent on " required!

call plug#end()




" =============================================================================
" General settings {{{
" =============================================================================
syntax enable " syntax highlight on

set modelines=0
set noexrc " dont use local version of .(g)vimrc, .exrc
set cpoptions=aABceFsmq
"             |||||||||
"             ||||||||+-- When joining lines, leave the cursor
"             |||||||      between joined lines
"             |||||||+-- When a new match is created (showmatch)
"             ||||||      pause for .5
"             ||||||+-- Set buffer options when entering the
"             |||||      buffer
"             |||||+-- :write command updates current file name
"             ||||+-- Automatically add <CR> to the last line
"             ||||     when using :@r
"             |||+-- Searching continues at the end of the match
"             ||      at the cursor position
"             ||+-- A backslash has no special meaning in mappings
"             |+-- :write updates alternative file name
"             +-- :read updates alternative file name

set autoread

" Load filetype plugins/indent settings

set autochdir " always switch to the current file directory
set backspace=indent,eol,start " make backspace a more flexible
set ttyfast

set title                " change the terminal's title

" set backup (and swap) files into sperate directory
" set backup " make backup files
" set backupdir=~/.vim/backup " where to put backup files
" set directory=~/.vim/tmp " directory to place swap files in
set nobackup
set noswapfile

" Make Vim use the system clipboard as the default register
" If your version of Vim supports both the +clipboard and +xterm_clipboard
" features, then you might prefer to use this setting instead:
if has('unnamedplus')
  set clipboard=unnamed,unnamedplus 
endif

set fileformats=unix,dos,mac " support all three, in this order
set hidden " you can change buffers without saving
" (XXX: #VIM/tpope warns the line below could break things)
set iskeyword+=_,$,@,%,# " none of these are word dividers

" Add mouse support - cursor position, selection tabs.
" (Use shift to select text.)
" n = normal mode, i = insert, c = command (do i use), a = all
" set mouse=ni
set mouse=a
" Mouse popup menu
set mousemodel=popup
" Toggle mouse
map   <F7> <Esc>:set mouse=<CR>
map <S-F7> <Esc>:set mouse=nic<CR>

set noerrorbells " don't make noise
set whichwrap=b,s,h,l,<,>,~,[,] " everything wraps
"             | | | | | | | | |
"             | | | | | | | | +-- "]" Insert and Replace
"             | | | | | | | +-- "[" Insert and Replace
"             | | | | | | +-- "~" Normal
"             | | | | | +-- <Right> Normal and Visual
"             | | | | +-- <Left> Normal and Visual
"             | | | +-- "l" Normal and Visual (not recommended)
"             | | +-- "h" Normal and Visual (not recommended)
"             | +-- <Space> Normal and Visual
"             +-- <BS> Normal and Visual
" ignore these list file extensions
" Tab Complete Menu
set wildmenu " turn on command line completion wild style
set wildignore+=*.o,*~,.lo,*.dll,*.obj,*.swp,*.class,*.bak,*.exe,*.pyc,*.jpg,*.gif,*.png
set suffixes+=.in,.a

set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo

set timeoutlen=350 " timeout when mapping keystrokes
set showfulltag " show full tag insert mode


" ========================== Text/Formatting Layout =========================
"
" Smart autoindenting when starting a new line
"set autoindent
"set smartindent

" Search
" Incremental Search - Searches begin immediately
set incsearch " BUT do highlight as you type you search phrase
set hlsearch "highlight search
set ignorecase
set shiftround " when at 3 spaces, and I hit > ... go to 4, not 5
set smartcase " if there are caps, go case-sensitive
set gdefault
set showmatch
set matchtime=1

" Use sane regexes
nnoremap / /\v
vnoremap / /\v

" Toggle the highlight search
"map <leader><space> :set hlsearch!<cr>
nmap <tab> %
vmap <tab> %

" set completeopt= " don't use a pop up menu for completions
set nowrap              " do not wrap line
set noswapfile
set linebreak
set formatoptions=croqnl1  " Automatically insert comment leader on return,
                           " and let gq format comments
set ignorecase  " case insensitive by default
set infercase   " case inferred by default

set textwidth=79  " lines longer than 79 columns will be broken
set shiftwidth=4  " operation >> indents 4 columns; << unindents 4 columns
set tabstop=4     " a hard TAB displays as 4 columns
set expandtab     " insert spaces when hitting TABs
set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set shiftround    " round indent to multiple of 'shiftwidth'
set autoindent    " align the new line indent with the previous line

" Wrap whole words - but forces line break
" set wm=2
set matchpairs+=<:>
set vb t_vb= " Turn off the bell

" Show line number when print
set printoptions+=number:y

" Folding
set foldenable          " Turn on folding
set foldmarker={,}      " Fold C style code (only use this as default if you
                        " use a high foldlevel)
set foldmethod=marker   " Fold on the marker
set foldlevel=100       " Don't autofold anything (but I can still fold manually)
set foldopen=block,hor,mark,percent,quickfix,tag " what movements open folds
function! SimpleFoldText()
    return getline(v:foldstart).' '
endfunction
set foldtext=SimpleFoldText() " Custom fold text function (cleaner than default)

" allows cursor change in tmux mode
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" ========================== Auto Command ===================================
" Automatically reload vimrc when it's saved
augroup reload_vimrc
    autocmd!
augroup END
au reload_vimrc BufWritePost .vimrc so ~/.vimrc

" Color Column (only on insert)
if exists('&colorcolumn')
    augroup vimrc
        autocmd!
    augroup END
     autocmd vimrc InsertEnter * set colorcolumn=79
     autocmd vimrc InsertLeave * set colorcolumn=""
endif

" ======================== Vim Mapping Keystroke ============================

" Type <Space>w to save file (a lot faster than :w<Enter>)
nnoremap <Leader>w :w<CR>

" sudo write
cmap ww w !sudo tee % > /dev/null

" Easy split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


" Don't using to arrow to moving
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Map Y behave like other capitals
map Y y$

" Disable search highlighting
nnoremap <silent> <Esc><Esc> :nohlsearch<CR><Esc>

" Keep search result at the center of screen
nmap n nzz
nmap N Nzz
nmap * *zz
nmap # #zz
nmap g* g*zz
nmap g# g#zz

" Select all text
noremap vA ggVG

" Open tig - only neovim
nnoremap <Leader>gg :tabnew<CR>:terminator tig<CR>

" Copy & paste to system clipboard with <Space>p and <Space>y:
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

nnoremap <Leader>y "*y
nnoremap <Leader>p "*p
nnoremap <Leader>P "*P

" Clipboard for Linux
nnoremap yy yy"+yy
vnoremap y ygv"+y
nmap gp "+gP

" Reselect visual block after indent/outdent
vnoremap < <gv
vnoremap > >gv

" Mapping ; to : to quickly type command
nnoremap ; :

" Use Q for formatting the current paragraph (or selection)
vmap Q gq
nmap Q gqap

" map Esc to another key to quickly
" mapping Esc to jj
ino jj <esc>
cno jj <C-c>
vno v <esc>

" Use jk as <Esc>
inoremap jk <Esc>
inoremap kj <Esc>

" Press i to enter insert mode, and ii to exit.
imap ii <Esc>

" Two semicolons are easy to type.
imap ;; <Esc>

" On gvim and Linux console Vim, you can use Alt-Space.
imap <M-Space> <Esc>

" Automatic retab and write
map <F8> :retab <CR> :w! <CR>

"Paste to ix.io
noremap <silent> <leader>i :w !ix<CR>

" <Ctrl-l> redraws the screen and removes any search highlighting.
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

" check file change every 4 seconds ('CursorHold')
" " and reload the buffer upon detecting change
set autoread
augroup vimrc
    autocmd!
augroup END
au vimrc CursorHold * checktime
