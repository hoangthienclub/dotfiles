# will work PS1 will be '$ '
#PS1=
#PS1="$PS1"'$([ -n "$TMUX" ] && tmux setenv TMUXPWD_$(tmux display -p "#I_#P") "$PWD")'
#PS1='$ '$PS1

# Path to your oh-my-zsh installation.
  export ZSH=/home/vunhan/.oh-my-zsh
  export TERM="screen-256color"

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="powerlevel9k/powerlevel9k"

POWERLEVEL9K_MODE='awesome-patched'
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon dir vcs)
POWERLEVEL9K_PROMPT_ON_NEWLINE=true

#POWERLEVEL9K_DISABLE_RPROMPT=true
POWERLEVEL9K_RAM_ELEMENTS=(ram_free)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status ram)

POWERLEVEL9K_OS_ICON_BACKGROUND="white"
POWERLEVEL9K_OS_ICON_FOREGROUND="blue"
POWERLEVEL9K_DIR_HOME_FOREGROUND="white"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="white"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="white"
# Colorize only the visual identifier
POWERLEVEL9K_LOAD_CRITICAL_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_LOAD_WARNING_VISUAL_IDENTIFIER_COLOR="yellow"
POWERLEVEL9K_LOAD_NORMAL_VISUAL_IDENTIFIER_COLOR="green"

POWERLEVEL9K_STATUS_VERBOSE=true

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-completions)
plugins+=(adb archlinux zsh-history-substring-search fbterm
		almostontop ansiweather solarized-man zsh-notify)
plugins+=(zsh-autosuggestions)

# User configuration

export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/opt/android-sdk/platform-tools:/opt/android-sdk/tools:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/vunhan/script:/home/vunhan/bin:/home/vunhan/.gem/ruby/2.3.0/bin"

#ARM dev-tools
export PATH="$PATH:/home/vunhan/Horical/arm/opt/FriendlyARM/toolschain/4.5.1/bin"

# Go
export GOPATH="/home/vunhan/project/go"
export PATH="$PATH:$GOPATH/bin"

# Config home = ~/.config
export XDG_CONFIG_HOME="$HOME/.config"

# TMUX weather
export TMUX_POWERLINE_SEG_WEATHER_LOCATION="1252431"

# fcitx env
export XMODIFIERS="@im=fcitx"                                                  
export GTK_IM_MODULE='fcitx'                                                    
export QT_IM_MODULE='fcitx'                                                      

export MYVIMRC="$HOME/.vimrc"

MESA_GL_VERSION_OVERRIDE=4.1COMPAT

# Proxy
export http_proxy=http://127.0.0.1:3128/
export https_proxy=$http_proxy
export ftp_proxy=$http_proxy
export rsync_proxy=$http_proxy
export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"

eval $( dircolors -b /home/vunhan/GITs/dircolors-solarized/dircolors.ansi-dark )

export VISUAL="vim"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
alias x="startx"
alias open="xdg-open"
alias x-www-browser="firefox"
alias tree1="tree -L 1"
alias tree2="tree -L 2"
alias tree3="tree -L 3"
alias my_best7z="7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on"

# POWERLEVEL9K THEME SETTING
POWERLEVEL9K_COLOR_SCHEME='dark'

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
